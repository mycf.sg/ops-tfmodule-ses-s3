provider "aws" {
  version = "~> 2.0"
  region = "${var.aws_ses_region}"
}

locals {
  email_addresses = "${formatlist("%s@%s", var.local_parts, replace(var.domain_name, "/\\.$/", ""))}"
  resource_prefix = "${lower(replace(replace(var.domain_name, "/\\./", "-"), "/\\-$/", ""))}"
}

data "aws_s3_bucket" "bucket_info" {
  bucket = "${var.s3_bucket_name}"
}

data "aws_route53_zone" "domain_info" {
  name         = "${var.domain_name}"
  private_zone = false
}

resource "aws_ses_domain_identity" "ses_domain_identity" {
  domain = "${data.aws_route53_zone.domain_info.name}"
}

resource "aws_route53_record" "ses_verification" {
  zone_id = "${data.aws_route53_zone.domain_info.zone_id}"
  name = "_amazonses.${data.aws_route53_zone.domain_info.name}"
  type= "TXT"
  ttl = "600"
  records = ["${aws_ses_domain_identity.ses_domain_identity.verification_token}"]
}

resource "aws_ses_receipt_rule_set" "ses_receipt_rule_set" {
  rule_set_name = "${local.resource_prefix}-tf"
}

resource "aws_ses_receipt_rule" "ses_receipt_rules" {
  count = "${length(local.email_addresses)}"
  rule_set_name = "${aws_ses_receipt_rule_set.ses_receipt_rule_set.rule_set_name}"
  name = "${replace(element(local.email_addresses, count.index), "/[\\.@\\_]/", "-")}"
  recipients = ["${element(local.email_addresses, count.index)}"]
  enabled = "${var.rule_enable}"
  scan_enabled = "${var.rule_enable_scan}"
  tls_policy = "${var.rule_tls_policy}"

  add_header_action {
    header_name = "ProcessedBy"
    header_value = "${aws_ses_receipt_rule_set.ses_receipt_rule_set.rule_set_name}_${replace(element(local.email_addresses, count.index), "/[\\.@\\_]/", "-")}"
    position = 1
  }

  s3_action {
    bucket_name = "${data.aws_s3_bucket.bucket_info.bucket}"
    object_key_prefix = "${replace(element(local.email_addresses, count.index), "/[\\.@\\_]/", "-")}"
    position = 2
  }

  # TODO: add in the other `actions_` input
}

resource "aws_ses_active_receipt_rule_set" "main" {
  rule_set_name = "${aws_ses_receipt_rule_set.ses_receipt_rule_set.rule_set_name}"
}
