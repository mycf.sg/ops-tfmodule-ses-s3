# SES

This module provisions listener email addresses given a registered domain name with Route53.

# Assumptions

- You have registered a domain using Route53
- The domain does not currently have any `MX` records
- You want to receive emails using an email address with that domain
- You want the emails to be put into storage using an S3 bucket

# Inputs

| Key | Description |
| --- | --- |
| `aws_ses_region` | Region where SES will be deployed |
| `domain_name` | Domain name to use for the email receiving functionality |
| `rule_enable` | The default 'enabled' parameter of the SES receipt rule |
| `rule_enable_scan` | The default 'scan_enabled' parameter of the SES receipt rule |
| `rule_tls_policy` | The default 'scan_enabled' parameter of the SES receipt rule |
| `local_parts` | Domain name to use for the email receiving functionality |
| `s3_bucket_name` | Where received mail goes to |
| `actions_add_header` | Additional add-header actions {header_name, header_value, position} (see https://www.terraform.io/docs/providers/aws/r/ses_receipt_rule.html) |
| `actions_bounce` | Additional bounce actions {message, sender, smtp_reply_code, status_code, topic_arn, position} (see https://www.terraform.io/docs/providers/aws/r/ses_receipt_rule.html) |
| `actions_lambda` | Additional lambda actions {function_arn, invocation_type, topic_arn, position} (see https://www.terraform.io/docs/providers/aws/r/ses_receipt_rule.html) |
| `actions_s3` | Additional s3 actions {bucket_name, kms_key_arn, object_key_prefix, topic_arn, position} (see https://www.terraform.io/docs/providers/aws/r/ses_receipt_rule.html) |
| `actions_sns` | Additional sns actions {topic_arn, position} (see https://www.terraform.io/docs/providers/aws/r/ses_receipt_rule.html) |
| `actions_stop` | Additional stop actions {scope, topic_arn, position} (see https://www.terraform.io/docs/providers/aws/r/ses_receipt_rule.html) |
| `actions_workmail` | Additional workmail actions {organization_arn, topic_arn, position} (see https://www.terraform.io/docs/providers/aws/r/ses_receipt_rule.html) |

# Outputs

| Key | Description |
| --- | --- |
| `domain_id` | ID of the selected domain name |
| `domain_name` | Selected domain name |
| `domain_nameservers` | List of name servers |
| `domain_private_zone` | Is the domain hosted in a private zone? |
| `domain_record_sets_count` | Number of record sets registered with the 'domain_name' |
| `domain_zone_id` | Zone ID of the specified domain name |
| `dns_record_content` | Content of the inserted DNS record |
| `dns_record_fqdn` | Fully Qualified Domain Name of the DNS record in Route53 |
| `dns_record_id` | ID of the DNS record in Route53 |
| `dns_record_ttl` | TTL of the DNS record in Route53 |
| `dns_record_type` | Type of the DNS record in Route53 |
| `ses_domain_arn` | ARN of the domain registered in SES |
| `ses_domain_id` | ID of the domain registered in SES |
| `ses_domain_verification_token` | Verification token for SES domain - should be the same as the | first entry in the 'dns_record_content' output |
| `ses_rule_set_id` | ID of the terraformed ruleset |
| `ses_rule_set_name` | Name of the terraformed ruleset |
| `ses_rule_names` | Names of the terraformed ruleset |
| `ses_rule_emails` | Email addresses of the terraformed ruleset |
| `s3_bucket_arn` | ARN of the S3 bucket being used |
| `s3_bucket_id` | ID of the S3 bucket being used |
| `s3_bucket_region` | Region of the S3 bucket being used |
