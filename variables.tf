variable "aws_ses_region" {
  description = "Region where SES will be deployed"
  default = "us-east-1"
  type = string
}

variable "domain_name" {
  description = "Domain name to use for the email receiving functionality"
  default = ""
  type = string
}

variable "rule_enable" {
  description = "The default 'enabled' parameter of the SES receipt rule"
  default = true
  type = bool
}

variable "rule_enable_scan" {
  description = "The default 'scan_enabled' parameter of the SES receipt rule"
  default = true
  type = bool
}

variable "rule_tls_policy" {
  description = "The default 'scan_enabled' parameter of the SES receipt rule"
  default = "Optional"
  type = string
}

variable "local_parts" {
  description = "Domain name to use for the email receiving functionality"
  default = [
    "admin",
    "aws",
    "azure",
    "bitbucket",
    "digitalocean",
    "dockerhub",
    "facebook",
    "gcp",
    "github",
    "gitlab",
    "google",
    "microsoft",
    "npm",
    "slack",
    "pivotal",
    "terraform",
    "trello",
    "twitter",
  ]
  type = list(string)
}

variable "s3_bucket_name" {
  description = "Where received mail goes to"
  default = ""
  type = string
}

variable "actions_add_header" {
  description = "Additional add-header actions {header_name, header_value, position} (see https://www.terraform.io/docs/providers/aws/r/ses_receipt_rule.html)"
  default = []
  type = object({
    header_name = string
    header_value = string
    position = number
  })
}

variable "actions_bounce" {
  description = "Additional bounce actions {message, sender, smtp_reply_code, status_code, topic_arn, position} (see https://www.terraform.io/docs/providers/aws/r/ses_receipt_rule.html)"
  default = []
  type = object({
    message = string
    sender = string
    smtp_reply_code = number
    status_code = number
    topic_arn = string
    position = number
  })
}

variable "actions_lambda" {
  description = "Additional lambda actions {function_arn, invocation_type, topic_arn, position} (see https://www.terraform.io/docs/providers/aws/r/ses_receipt_rule.html)"
  default = []
  type = object({
    function_arn = string
    invocation_type = string
    topic_arn = string
    position = number
  })
}

variable "actions_s3" {
  description = "Additional s3 actions {bucket_name, kms_key_arn, object_key_prefix, topic_arn, position} (see https://www.terraform.io/docs/providers/aws/r/ses_receipt_rule.html)"
  default = []
  type = object({
    bucket_name = string
    kms_key_arn = string
    object_key_prefix = string
    topic_arn = string
    position = number
  })
}

variable "actions_sns" {
  description = "Additional sns actions {topic_arn, position} (see https://www.terraform.io/docs/providers/aws/r/ses_receipt_rule.html)"
  default = []
  type = object({
    topic_arn = string
    position = number
  })
}

variable "actions_stop" {
  description = "Additional stop actions {scope, topic_arn, position} (see https://www.terraform.io/docs/providers/aws/r/ses_receipt_rule.html)"
  default = []
  type = object({
    scope = string
    topic_arn = string
    position = number
  })
}

variable "actions_workmail" {
  description = "Additional workmail actions {organization_arn, topic_arn, position} (see https://www.terraform.io/docs/providers/aws/r/ses_receipt_rule.html)"
  default = []
  type = object({
    organization_arn = string
    topic_arn = string
    position = number
  })
}
