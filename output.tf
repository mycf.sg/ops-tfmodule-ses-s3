output "domain_id" {
  description = "ID of the selected domain name"
  value = "${data.aws_route53_zone.domain_info.id}"
}

output "domain_name" {
  description = "Selected domain name"
  value = "${data.aws_route53_zone.domain_info.name}"
}

output "domain_nameservers" {
  description = "List of name servers"
  value = "${data.aws_route53_zone.domain_info.name_servers}"
}

output "domain_private_zone" {
  description = "Is the domain hosted in a private zone?"
  value = "${data.aws_route53_zone.domain_info.private_zone}"
}

output "domain_record_sets_count" {
  description = "Number of record sets registered with the 'domain_name'"
  value = "${data.aws_route53_zone.domain_info.resource_record_set_count}"
}

output "domain_zone_id" {
  description = "Zone ID of the specified domain name"
  value = "${data.aws_route53_zone.domain_info.zone_id}"
}

output "dns_record_content" {
  description = "Content of the inserted DNS record"
  value = "${aws_route53_record.ses_verification.records}"
}

output "dns_record_fqdn" {
  description = "Fully Qualified Domain Name of the DNS record in Route53"
  value = "${aws_route53_record.ses_verification.fqdn}"
}

output "dns_record_id" {
  description = "ID of the DNS record in Route53"
  value = "${aws_route53_record.ses_verification.id}"
}

output "dns_record_ttl" {
  description = "TTL of the DNS record in Route53"
  value = "${aws_route53_record.ses_verification.ttl}"
}

output "dns_record_type" {
  description = "Type of the DNS record in Route53"
  value = "${aws_route53_record.ses_verification.type}"
}

output "ses_domain_arn" {
  description = "ARN of the domain registered in SES"
  value = "${aws_ses_domain_identity.ses_domain_identity.arn}"
}

output "ses_domain_id" {
  description = "ID of the domain registered in SES"
  value = "${aws_ses_domain_identity.ses_domain_identity.id}"
}

output "ses_domain_verification_token" {
  description = "Verification token for SES domain - should be the same as the first entry in the 'dns_record_content' output"
  value = "${aws_ses_domain_identity.ses_domain_identity.verification_token}"
}

output "ses_rule_set_id" {
  description = "ID of the terraformed ruleset"
  value = "${aws_ses_receipt_rule_set.ses_receipt_rule_set.id}"
}

output "ses_rule_set_name" {
  description = "Name of the terraformed ruleset"
  value = "${aws_ses_receipt_rule_set.ses_receipt_rule_set.rule_set_name}"
}

output "ses_rule_names" {
  description = "Names of the terraformed ruleset"
  value = [
    for id, instance in aws_ses_receipt_rule.ses_receipt_rules:
    instance.name
  ]
}

output "ses_rule_emails" {
  description = "Email addresses of the terraformed ruleset"
  value = [
    for id, instance in aws_ses_receipt_rule.ses_receipt_rules:
    join(",", instance.recipients)
  ]
}

output "s3_bucket_arn" {
  description = "ARN of the S3 bucket being used"
  value = "${data.aws_s3_bucket.bucket_info.arn}"
}

output "s3_bucket_id" {
  description = "ID of the S3 bucket being used"
  value = "${data.aws_s3_bucket.bucket_info.id}"
}

output "s3_bucket_region" {
  description = "Region of the S3 bucket being used"
  value = "${data.aws_s3_bucket.bucket_info.region}"
}

######################################################################################
# for debugging purposes
######################################################################################

# output "debug_local_email_addresses" {
#   description = "Outputs all locals for debugging purposes"
#   value = "${local.email_addresses}"
# }

# output "debug_local_resource_prefix" {
#   description = "Outputs all locals for debugging purposes"
#   value = "${local.resource_prefix}"
# }

# output "debug_data_bucket_info" {
#   description = "Selected S3 bucket details for debugging purposes"
#   value = "${data.aws_s3_bucket.bucket_info}"
# }

# output "debug_data_domain_info" {
#   description = "Selected Route53 details for debugging purposes"
#   value = "${data.aws_route53_zone.domain_info}"
# }

# output "debug_resource_ses_receipt_rules" {
#   description = "Created receipt rules for debugging purposes"
#   value = "${aws_ses_receipt_rule.ses_receipt_rules}"
# }
